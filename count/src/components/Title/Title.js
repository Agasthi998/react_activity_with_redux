import React from 'react'
import './Title.scss'

export default function Title() {
    return (
        <div className="title">
            <h1>React Counter</h1>
        </div>
    )
}
