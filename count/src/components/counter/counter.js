import React from "react";
import { useSelector } from "react-redux";
import "./counter.scss";

const Counter = () => {
  const Counter = useSelector((state) => state.counter);

  return (
    <div className="counter">
      <h1>Count: {Counter.count}</h1>
    </div>
  );
};

export default Counter;
