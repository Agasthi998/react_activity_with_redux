import React from "react";
import { useDispatch } from "react-redux";
import { increaseCounter, decreaseCounter } from "../../Actions/countActions";
import "./action.scss";

const Action = () => {
  const dispatch = useDispatch();

  const plusBtn = () => {
    dispatch(increaseCounter());
  };

  const minusBtn = () => {
    dispatch(decreaseCounter());
  };

  return (
    <div className="container">
      <div className="buttons">
        <button onClick={plusBtn}>+</button>
        <button onClick={minusBtn}>-</button>
      </div>
    </div>
  );
};

export default Action;
