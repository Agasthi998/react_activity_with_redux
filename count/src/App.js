import "./App.scss";
import Action from "./components/action/action";
import Count from "./components/counter/counter";
import Title from "./components/Title/Title";

function App() {
  return (
    <div className="App">
      <Title />
      <Count />
      <Action />
    </div>
  );
}

export default App;
