import { createStore, combineReducers } from "redux";
import { counterReducer } from "./Reducers/countReducer";
import { composeWithDevTools } from "redux-devtools-extension";

const reducer = combineReducers({
  counter: counterReducer,
});

const store = createStore(reducer, composeWithDevTools());

export default store;
