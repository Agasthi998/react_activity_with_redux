import { INCREASE_COUNT, DECREASE_COUNT } from "../Constants/countConstants";

export const increaseCounter = () => {
  return {
    type: INCREASE_COUNT,
    payload: "increase",
  };
};

export const decreaseCounter = () => {
  return {
    type: DECREASE_COUNT,
    payload: "decrease",
  };
};
